(function ()
{
    'use strict';

    angular
        .module('fuse')
        .controller('IndexController', IndexController);

    /** @ngInject */
    function IndexController(fuseTheming)
    {
        var vm = this;
        // This Makes Full screen on first click!
        //Commented out for testing purposes
        //document.onclick = function (argument) {
          //  var conf = true;
           // var docelem = document.documentElement;

            //if (conf == true) {
               // if (docelem.requestFullscreen) {
                  //  docelem.requestFullscreen();
              //  }
              //  else if (docelem.mozRequestFullScreen) {
                 //   docelem.mozRequestFullScreen();
               // }
               // else if (docelem.webkitRequestFullScreen) {
                 //   docelem.webkitRequestFullScreen();
              //  }
               // else if (docelem.msRequestFullscreen) {
                 //   docelem.msRequestFullscreen();
                //}
          //  }
       // }
        // Data
        vm.themes = fuseTheming.themes;

        //////////
    }
})();