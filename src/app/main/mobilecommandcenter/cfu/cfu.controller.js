(function ()
{
    'use strict';

    angular
        .module('app.mobile')
        .controller('CFUController', CFUController);

    /** @ngInject */
    function CFUController(QuestionData)
    {
        var vm = this;

        // Data
        vm.helloText = QuestionData.data;
        vm.background = QuestionData.data.Background;
        vm.data = QuestionData.data.English;
        vm.questions = [];

        // Methods
           var CFU = {
                name: "",
                branchId : $routeParams.branchId,
                orchestravisitId : [],
                orchestraTicketNumber : $scope.ticketNumber,
                questions : vm.questions,
                answers : surveyResponse,
                note: ""
                };
        //////////
        vm.welcomeScreenURL = QuestionData.data.WelcomeScreenURL
        vm.showForm = false;
        vm.showProgress = true;

        //vm.branchId = $routeParams.branchId;
        //vm.visitId = $routeParams.visitId;
        var englishQ = [
        {
            type: 'radio',
            Id: 'q1',
            Question: 'Did the Advisor greet you?',
            Answer: '',
            Options: [
                { value: 'Yes', db: '1' },
                { value: 'No', db: '0' }
            ]
        },
        {
            type: 'radio',
            Id: 'q2',
            Question: 'Was the advisor polite and friendly?',
            Answer: '',
            Options: [
                { value: 'Yes', db: '1' },
                { value: 'No', db: '0' }
            ]
        },
        {
            type: 'radio',
            Id: 'q3',
            Question: 'Was the reason for your visit:',
            Answer: '',
            Options: [
                { value: 'Resolved', db: '1' },
                { value: 'Unresolved', db: '0' }
            ]
        },
        {
            type: 'radio',
            Id: 'q4',
            Question: 'How would you rate the waiting time for your service?',
            Answer: '',
            Options: [
                { value: 'Acceptable', db: '1' },
                { value: 'Unacceptable', db: '0' }
            ]
        },
        {
            type: 'dropdown',
            Id: 'dropdown1',
            Question: 'Based on your experience today, how likely are you to recommend MTN to friend or relative on a scale of 0 - 10. (0 being very unlikely and 10 very likely)',
            Answer: '',
            Default: 'Not Selected',
            Options: [
                { value: '0', db: '0' },
                { value: '1', db: '1' },
                { value: '2', db: '2' },
                { value: '3', db: '3' },
                { value: '4', db: '4' },
                { value: '5', db: '5' },
                { value: '6', db: '6' },
                { value: '7', db: '7' },
                { value: '8', db: '8' },
                { value: '9', db: '9' },
                { value: '10', db: '10' }
            ]
        }
        ];

    }
})();
