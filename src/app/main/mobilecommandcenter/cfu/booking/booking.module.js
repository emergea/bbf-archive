﻿(function () {
    'use strict';

    angular
    .module('app.mobile.booking', [])
    .config(config);

    function config($stateProvider, msApiProvider, msNavigationServiceProvider) {
        $stateProvider
                .state('app.mobile.booking', {
                    url: '/booking',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/mobilecommandcenter/booking/booking.html',
                            controller: 'BookingController as vm'
                        }
                    },
                    resolve: {
                        MobileData: function (msApi) {
                            return msApi.resolve('mobile@get');
                        }
                    }
                })

        // Api
        msApiProvider.register('mobile', ['app/data/qmatic/data.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('fuse.mobile.booking', {
            title: 'Mobile',
            icon: 'icon-tile-four',
            state: 'app.mobile',
            weight: 1
        });
    }
})();