﻿(function () {
    'use strict';

    angular
        .module('app.mobile')
        .controller('OrchestraController', OrchestraController);

    function OrchestraController($rootScope, $mdDialog, MobileData, $location, api, $timeout, userService, $interval) {

        var vm = this;

        //Data        
        vm.mobileData = MobileData.data;                
        vm.branchId = $location.search().branchId;
        vm.stepId = $location.search().stepId;
        vm.visitId = $location.search().visitId;
        vm.queueId = $location.search().queueId;
        vm.entryPointId = $location.search().entryPoints;
        vm.selectedQueue = '';
        vm.myInterval;
        vm.file = {
            name: '',
            size: ''
        };

        vm.files = [];

        vm.customer = {
            firstName: '',
            lastName: '',
            cardNumber: '',
            id: '',
            properties: {
                mobile: ''
            }
        };

        vm.visits = {
            id: '',
            ticketId: ''
        };

        vm.queue = {
            id: '',
            name: '',
            customersWaiting: 0,
            waitingTime: 0
        }

        vm.customers = [];
        vm.newCustomer = false;
        vm.selectedService = '';
        vm.called = false;

        //Steps
        vm.views = {
            step1: 'app/main/mobilecommandcenter/orchestra/views/steps/step1.html',
            step2: 'app/main/mobilecommandcenter/orchestra/views/steps/step2.html',
            step3: 'app/main/mobilecommandcenter/orchestra/views/steps/step3.html',
            step4: 'app/main/mobilecommandcenter/orchestra/views/steps/step4.html',
            step5: 'app/main/mobilecommandcenter/orchestra/views/steps/step5.html',
        };

        vm.currentView = 'step1';        

        if(vm.stepId > 1)
        {
            vm.currentView = 'step' + vm.stepId;
        }
        

        //Methods
        vm.addNewCustomer = addNewCustomer;
        vm.scanner = scanner;
        vm.addDocuments = addDocuments;
        vm.makeBooking = makeBooking;
        vm.transfer = transfer;
        vm.comment = comment;
        vm.help = help;
        vm.changeView = changeView;
        vm.onSwipeDown = onSwipeDown;
        vm.showTicket = false;
        vm.date = new Date();
        vm.callTicket = callTicket;
        vm.removeDocument = removeDocument;

        //Ticket swipe action
        function onSwipeDown(ev) {
            vm.showTicket = true;
            VisitApiController();
            $timeout(function () {
                vm.changeView('step4');
            }, 5000);
        };

        //API
        MobileDataApiController();

        //Animation
        vm.isShow = false;
        vm.animation = "flip-in";

        $timeout(function () {
            vm.isShow = true;
        }, 1000);

        //Timer and Format
        vm.formatTime = formatTime;
        vm.waitingTime = 0;

        function setWaitingTime() {
            $interval(function () {
                angular.forEach(vm.mobileData.queueMapping, function (value, key) {
                    if (value.waitingTime != 0) {
                        value.waitingTime = value.waitingTime + 1;
                    }
                })},1000);
        };

        //Format time
        function formatTime(secs) {
            if (secs === undefined) {
                return "00:00";
            }
            else {
                var t = new Date(1970, 0, 1);
                t.setSeconds(secs);
                var s = t.toTimeString().substr(0, 8);
                if (secs > 86399)
                    s = Math.floor((t - Date.parse("1/1/70")) / 3600000) + s.substr(2);
                return s;
                return 0;
            }
        }

        setWaitingTime();

        $interval(function () {
            MobileDataApiController();
        }, 60000);
        
        function exists(item, list) {
            return list.indexOf(item) > -1;
        }

        //API  Queues, Services, Branch
        function MobileDataApiController() {

            //branch
            api.mobile.getBranch.get
            (
                //Params
                { branchId: vm.branchId },

                //Success response
                function (response) {
                    console.log(response);
                    vm.mobileData.branch = response;
                },

                //Failure response
                function (response) {
                    console.error(response);
                }
            );

            //branch queues
            api.mobile.getBranchQueques.query
            (
                //Params
                { branchId: vm.branchId },

                //Success response
                function (response) {
                    console.log(response);
                    vm.mobileData.queues = response;
                },

                //Failure response
                function (response) {
                    console.error(response);
                }
            );

            //branch queue
            api.mobile.getBranchQueque.query
            (
                //Params
                { branchId: vm.branchId, queueId: vm.queueId },

                //Success response
                function (response) {
                    console.log(response);
                    vm.mobileData.queue = response;
                },

                //Failure response
                function (response) {
                    console.error(response);
                }
 
            );

            //branch services
            api.mobile.getBranchServices.query
            (
                //Params
                { branchId: vm.branchId },

                //Success response
                function (response) {
                    console.log(response);
                    vm.mobileData.services = response;
                    getQueueInformation();
                },

                //Failure response
                function (response) {
                    console.error(response);
                    getQueueInformation();
                }

            );


            function getQueueInformation() {
                angular.forEach(vm.mobileData.services, function (value, key) {
                    //console.log("Service : " + value.internalName + "Service Id : " + value.id);
                    angular.forEach(vm.mobileData.queueMapping, function (value2, key2) {
                        if (exists(value.id, value2.services)) {
                            //console.log("We have a match");
                            angular.forEach(vm.mobileData.queues, function (value3, key3) {
                                if (value2.queueId == value3.id) {
                                    value2.waitingTime = value3.waitingTime;
                                    value2.customersWaiting = value3.customersWaiting;
                                }
                            })
                        }
                    });
                });
            }


            ////branch visits
            //api.mobile.getBranchVisits.query
            //(
            //    //Params
            //    { branchId: vm.branchId },

            //    //Success response
            //    function (response) {
            //        console.log(response);
            //        vm.mobileData.visits = response;
            //    },

            //    //Failure response
            //    function (response) {
            //        console.error(response);
            //    }
            //);
        }

        //API  Visit
        function VisitApiController() {

            api.mobile.createBranchVisit.post
            (
                //Params
                { branchId: vm.branchId, entryPointId: vm.entryPointId }, { services: [vm.selectedService.id] },

                //Success response
                function (response) {
                    console.log(response);
                    vm.mobileData.visit = response;

                    if (vm.customer.id != ''){
                        vm.mobileData.visit.customerIds.push(vm.customer.id);
                    }

                    $rootScope.$broadcast('set-user-visit', { "visit": vm.mobileData.visit });
                    userService.setVisitDetails(vm.mobileData.visit.appointmentId, vm.mobileData.visit.totalWaitingTime, vm.mobileData.visit.appointmentTime, vm.mobileData.visit.visitId, vm.mobileData.visit.waitingTime, vm.mobileData.visit.ticketId);

                    getBranchVisit(vm.mobileData.visit);
                    vm.queue.customersWaiting += 1;
                    vm.mobileData.queueMapping[service.id - 1].customersWaiting + 1;
                },
                 

                //Failure response
                function (response) {
                    console.error(response);
                    if (vm.mobileData.visit != "") {
                    vm.queue.customersWaiting += 1;
                };
                    
                }
            );
        };

        //Add new Customer
        function addNewCustomer() {
            if (!vm.customer) {

                //vm.customer = customer;
                vm.newCustomer = true;
            }
            vm.customers.push(vm.customer);
            api.mobile.saveNewCustomer.query
            (
                //Params
                {
                    'firstName': vm.customer.firstName,
                    'lastName': vm.customer.lastName,
                    'cardNumber': vm.customer.cardNumber,
                    'id': vm.customer.id,
                    'properties': {
                        'mobile': vm.customer.properties.mobile
                    }
                },

                //Success response
                function (response) {
                    console.log(response);   
                },

                //Failure response
                function (response) {
                    console.error(response);
                   
                }

            );

            vm.changeView('step3', vm.selectedService);
            $rootScope.$broadcast('set-user', { "customer": vm.customer });
            userService.setUserDetails(vm.customer.firstName, vm.customer.lastName, vm.customer.cardNumber, vm.customer.id, vm.customer.mobile);
        };

        //Get visit details
        function getBranchVisit(visit) {
            //loop through get visit call till the customer gets called
            
               vm.myInterval = $interval(function () {
                   console.log("visit id", visit.id);
                    //branch visit
                    api.mobile.getBranchVisit.get
                    (
                     
                        //Params
                        { branchId: vm.branchId, visitId: visit.id },

                        //Success response
                        function (response) {
                            console.log(response);
                            vm.mobileData.visit = response;
                            //$rootScope.$broadcast('set-visit', { "visits": vm.visits });
                            //userService.setVisitDetails(vm.visits.id, vm.visits.ticketId); 
                        },

                        //Failure response
                        function (response) {
                            console.error(response);
                        }
                    );

                    callTicket();
                }, 1000);

        }

        //check for ticket call
        function callTicket() {
            if (vm.mobileData.visit != undefined || vm.mobileData.visit.Id != '') {
                if (vm.mobileData.visit.timeSinceCalled > 0) {
                    vm.called = true;
                    $interval.cancel(vm.myInterval);
                }
            }         
        }



        //Delete customer
        function deleteCustomer() {
            vm.customers.unshift(1);
        };

        //Scanner window
        function scanner(ev) {

            $mdDialog.show(
                $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Scan')
                    .htmlContent('Scan Documents')
                    .ariaLabel('Scan Documents')
                    .ok('Close')
                    .targetEvent(ev)
                    );
        };

        //Add documents window
        function addDocuments() {

            angular.element(document.querySelector('#fileInput')).click();
            
            if (vm.files.length == 0) {

                angular.element(document.querySelector('#fileInput')).on('change', function (evt) {

                    vm.file = $(evt.currentTarget).get(0).files;

                    if (vm.file.length > 0) {
                        vm.files.push(vm.file[0]);
                    }

                });
            }


        };
        function removeDocument(index) {
<<<<<<< HEAD
            console.log("Removing Document" + index);
            if (vm.files.length != 0) {
                vm.files.splice(index,1);
=======
            if (vm.files.length != 0) {
                  vm.files.slice(index,1);
>>>>>>> 8936114ce37c3d78d010f99bc593c4e9fb6f4107
            }
        }

        //Make booking window
        function makeBooking(ev) {

            $mdDialog.show(
                $mdDialog.alert()
                .controller('BookingController')
                .controllerAs('vm')
                .templateUrl('app/main/mobilecommandcenter/booking/booking.html')
                .parent(angular.element($document.find('#content-container')))
                .targetEvent(ev)
                .clickOutsideToClose(true)
            );
        };

        //Transfer window
        function transfer(ev) {

            $mdDialog.show(
                $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Transfer')
                    .htmlContent('Transfer')
                    .ariaLabel('Transfer')
                    .ok('Close')
                    .targetEvent(ev)
           );
        };

        //Comment window
        function comment(ev) {

            $mdDialog.show(
                $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Comment')
                    .templateUrl('app/main/mobilecommandcenter/orchestra/comment/comment.html')
                    .parent(angular.element($document.find('#content-container')))
                    .ariaLabel('Comment')
                    .ok('Close')
                    .targetEvent(ev)
           );
        };

        //Help window
        function help(ev) {

            $mdDialog.show(
                $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Help')
                    .htmlContent('Help')
                    .ariaLabel('Help')
                    .ok('Close')
                    .targetEvent(ev)
           );
        };

        //Switch bew
        function changeView(view, service, queueId)
        {
            if (vm.views[view])
            {
                vm.currentView = view;
                vm.stepId = view.replace("step","")               
            }

            if(vm.branchId == undefined)
            {
                vm.branchId = 1;
            }

            if (service != undefined)
            {
                vm.selectedService = service;                
            }

            if (queueId != undefined) {
                angular.forEach(vm.mobileData.queues, function (value, key) {
                    if (value.id == queueId) {
                        vm.queue = value;
                        $rootScope.$broadcast('set-queue', { "queue": vm.queue });
                        userService.setQueueDetails(vm.queue.id, vm.queue.name, vm.queue.customersWaiting, vm.queue.waitingTime);
                    }
                })
            }

        }
    }

})();