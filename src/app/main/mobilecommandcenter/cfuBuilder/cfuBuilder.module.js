(function ()
{
    'use strict';

    angular
        .module('app.mobile.cfuBuilder', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
            .state('app.cfuBuilder', {
                url: '/cfubuilder',
                views  : {
                    'content@app': {
                        templateUrl: 'app/main/mobilecommandcenter/cfucBuilder.html',
                        controller : 'CFUBUILDERController as vm'
                    }
                },
                resolve: {
                    QuestionData: function (msApi)
                    {
                        return msApi.resolve('cfu@get');
                    }
                }
            });

        // Translation
        //$translatePartialLoaderProvider.addPart('app/main/sample');

        // Api
        msApiProvider.register('cfu', ['app/data/cfu/questions.json']);

        // Navigation
        //msNavigationServiceProvider.saveItem('fuse', {
        //    title : 'SAMPLE',
        //    group : true,
        //    weight: 1
        //});

        msNavigationServiceProvider.saveItem('fuse.cfu', {
            title    : 'Customer Feedback Solution',
            icon     : 'icon-tile-four',
            state: 'app.cfuBuilder',
            /*stateParams: {
                'param1': 'page'
             },*/
            weight   : 1
        });
    }
})();