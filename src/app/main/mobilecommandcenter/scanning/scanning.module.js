﻿(function () {
    'use strict';

    angular
    .module('app.mobile.scanning', [])
    .config(config);

    function config($stateProvider, msApiProvider, msNavigationServiceProvider) {
        $stateProvider
                .state('app.mobile.scanning', {
                    url: '/scanning',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/mobilecommandcenter/scanning/scanning.html',
                            controller: 'ScanningController as vm'
                        }
                    },
                    resolve: {
                        MobileData: function (msApi) {
                            return msApi.resolve('mobile@get');
                        }
                    }
                })

        // Api
        msApiProvider.register('mobile', ['app/data/qmatic/data.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('fuse.mobile.scanning', {
            title: 'Scanning',
            icon: 'icon-tile-four',
            state: 'app.mobile',
            weight: 1
        });
    }
})();