﻿(function ()
{
    'use strict';

    angular
        .module('app.mobile.scanning')
        .controller('ScanningController', ScanningController);

    function ScanningController(scanningData, $state) {
       
        var vm = this;

        //Data
        vm.scanningData = ScanningData.data;

    }            
})();