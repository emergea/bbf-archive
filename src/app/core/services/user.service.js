﻿(function ()
{
    'use strict';

    angular
        .module('app.core')
        .factory('userService', userService);


    /** @ngInject */
    function userService(api)
    {
        
        var vm = this;
        vm.userExist = false;
        vm.userDetails = {};
        vm.visitDetails = {};
        vm.queueDetails = {};
        vm.userVisitDetails = {};
        vm.setUserDetails = setUserDetails;
        vm.getUserDetails = getUserDetails;
        vm.setVisitDetails = setVisitDetails;
        vm.getVisitDetails = getVisitDetails;
        vm.setQueueDetails = setQueueDetails;
        vm.getQueueDetails = getQueueDetails;
        vm.setUserVisit = setUserVisit;
        vm.getUserVisit = getUserVisit;

        function setUserDetails(firstName, lastName, cardNumber, id, mobile) {
            vm.userDetails = { "firstName": firstName, "lastName": lastName, "cardNumber":cardNumber, "id":id, "mobile": mobile }
            vm.userExist = true;
        }

        function getUserDetails() {
            return vm.userDetails;
        }

        function setVisitDetails(id, ticketId) {
            vm.visitDetails = { "id": id, "ticketId": ticketId }
        }

        function getVisitDetails() {
            return vm.visitDetails;
        }

        function setQueueDetails(id, name, customersWaiting, waitingTime) {
            vm.queueDetails = { "id": id, "name": name, "customersWaiting": customersWaiting, "waitingTime": waitingTime }
        }

        function getQueueDetails() {
            return vm.queueDetails;
        }

        function setUserVisit(id, name, customersWaiting, waitingTime) {
            vm.userVisitDetails = { "appointmentId": appointmentId, "totalWaitingTime": totalWaitingTime, "appointmentTime": appointmentTime, "visitId": visitId, "waitingTime": waitingTime, "ticketId": ticketId }
        }

        function getUserVisit() {
            return vm.userVisitDetails;
        }

        return vm;
    }

})();