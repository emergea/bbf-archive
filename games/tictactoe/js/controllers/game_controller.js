'use strict';

function GameController($scope, game, grid_size)
{    
    $scope.grid = new Array(grid_size);
    $scope.status_message = "";
    $scope.computer_first = false;
    $scope.game_over = false;
    $scope.highScore = 0;//Whaat is the current highest score for the gamer.
    $scope.gameScore = 0;// what is the current score.
    $scope.ishighScoreBeingAccured;//after each successfull game accrude the score    
    $scope.donePlaying = false;//has the gamer at least played and finished one game?
    $scope.lastGamePlayed = "tictactoe";//indicates the game played to the returning url.

    $scope.gup = function(name)
    {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( window.location.href );
        if( results == null )
        {
            return "";
        }
        else
        {
            return results[1];
        }
    }

    $scope.loadJSON = function(callback)
    {
        var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
        xobj.open('GET', 'config/config.json', true); // Replace 'my_data' with the path to your file
        xobj.onreadystatechange = function ()
        {
            if (xobj.readyState == 4 && xobj.status == "200")
            {
                    // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
                    callback(xobj.responseText);
            }
        };
        xobj.send(null);
    }

    //We want to load the config as soon as the page loads. 
    $scope.loadJSON(function(response)
    {
        var config = JSON.parse(response);        
        $scope.config = config;        
    });   

    $scope.branchId =  $scope.gup('branchId');
    $scope.stepIdParam =  $scope.gup( 'stepId');

    $scope.startGame = function()
    {
        //only on loss and the restrated show to the gamer the score reset.
        //NB: if you do it on loss indication the scores can not be compared.
        if(!$scope.ishighScoreBeingAccured)
        {
            $scope.gameScore = 0;
        }
        
        $scope.status_message = "";
        $scope.game_over = false;
    	game.start($scope.grid.length, $scope.computer_first);
        $scope.status_message = "game started";
    }

    $scope.makeMove = function(col, row)
    {
        var boardIndex, symbol, winner;
        boardIndex = (row * grid_size) + col;
        if(game.board && game.board.canMove(boardIndex) && !game.winner && !game.tie)
        {
            // make move
            game.move(boardIndex);
            $scope.gameScore = $scope.gameScore + Math.floor((Math.random() * 10) + 1);

            // check winner
            if(game.winner)
            {
                if(game.winner === game.board.Q)
                {
                    $scope.ishighScoreBeingAccured = false;
                    $scope.status_message = "you lose!";
                }
                if(game.winner === game.board.E)
                {
                     $scope.ishighScoreBeingAccured = true;
                     $scope.status_message = "you win!";
                }
                $scope.game_over = true;
            }

            // check tie
            if(game.tie)
            {
                $scope.ishighScoreBeingAccured = true;
                $scope.status_message = "tie! no one wins!";
                $scope.game_over = true;
            }
            
            //Reset highScore accumulation, and replace/display the highScore if needed.        
            if($scope.gameScore > $scope.highScore)
            {
                $scope.highScore = $scope.gameScore;
            }
            
            //After a single game we can return the game, before then the score whould be zero and is not needed.
            $scope.donePlaying = true;
        }
    }

    $scope.getSquareSymbol = function(col, row)
    {
        var boardIndex = (row * grid_size) + col;
        return game.board ? game.board.renderSquare(boardIndex) : "";
    }

    $scope.isSquareInWinningCombo = function(col, row)
    {
        var boardIndex;
        if(game.board && game.winner && game.board.winning_combo)
        {
            boardIndex = (row * grid_size) + col;
            return game.board.winning_combo.indexOf(boardIndex) > -1;
        }
        return false;
    }

    $scope.endGame = function()
    {
        document.location = $scope.config.data.baseUrl  + "?stepIdParam=" + $scope.stepIdParam + "&branchIdParam=" + $scope.branchId + "&donePlaying=" + $scope.donePlaying + "&highScore=" + $scope.highScore + "&lastGamePlayed=" + $scope.lastGamePlayed;
    }
}
